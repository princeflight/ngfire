import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAjHdwTQucRaclRENVVktTC4MIp8Ts1SwY'
    }),
  ],
  exports: [
    FormsModule,
    AgmCoreModule
  ]
})
export class SharedModule { }
